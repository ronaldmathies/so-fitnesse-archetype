#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
!*> General Template Library
!-
    <b>Please note the following:</b>

    <b>Don't make any changes on this file or on any of the recursive folders and files.</b>

    Changes are to be made withing the so-fitnesse-archetype so that every project has
    the advantage of new or improved scenarios.
-!
*!

!contents -R2 -g -p -f -h
