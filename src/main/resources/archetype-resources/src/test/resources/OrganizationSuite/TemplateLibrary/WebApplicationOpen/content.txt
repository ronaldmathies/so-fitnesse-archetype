#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
!3 Load selenium server properties
!*> Load selenium server properties
!|script|
|start|PropertiesFixture|selenium.properties|
|${symbol_dollar}webdriver_browser=|get string property|webdriver.browser      |
*!

!3 Load application properties
!*> Load application properties
!|script|
|start|PropertiesFixture|application.properties|
|${symbol_dollar}$application_title=|get string property|application.title|
|${symbol_dollar}$application_url=  |get string property|application.url  |
*!

!3 Open the application
!*> Open the application
!|script|
|open web application; |${seleniumServerBrowser} |${application.url} |${application.name}|
*!