#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.fitnesse.debug;

import org.junit.Test;

/**
 * @author Ronald Mathies
 */
public class FitnesseJUnitRunner extends nl.sodeso.fitnesse.FitnesseJUnitRunner {

    @Test
    public void checkDateFixture() {
        checkFitnesseSinglePage("TestSuite.DateFixture");
    }

    @Test
    public void checkStartApplication() {
        checkFitnesseSinglePage("TestSuite.StartApplication");
    }

    public String getModuleName() {
        return "${artifactId}";
    }
}
